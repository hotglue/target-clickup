"""Clickup target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_clickup.sinks import (
    ClickupSink,
)


class TargetClickup(Target):
    """Sample target for Clickup."""

    name = "target-clickup"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=True
        ),
        th.Property(
            "list_id",
            th.StringType,
            required=False
        ),
    ).to_dict()
    default_sink_class = ClickupSink

if __name__ == '__main__':
    TargetClickup.cli()
