"""Clickup target sink class, which handles writing streams."""


from singer_sdk.sinks import RecordSink
import requests
import time
from dateutil.parser import parse
from datetime import datetime

class ClickupSink(RecordSink):
    """Clickup target sink class."""
    @property
    def get_headers(self):
        headers = {}
        headers['Authorization'] = f"Bearer {self.config.get('access_token')}"
        headers['Content-Type'] = "application/json"
        return headers
        

    def _request(self,record,stream,request_type='POST'):
        url = f"https://api.clickup.com/api/v2/{stream}"
        res = requests.request(method=request_type,url=url,json=record,headers=self.get_headers)
        res.raise_for_status()
        res = res.json()
        return res

    def get_assigness(self,assigness_list):    
        return_list = []
        for asignee in assigness_list:
            #implement lookup later
            return_list.append(asignee['id'])
        return return_list    
        
    # TODO: write model and convert target to new format. 
    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        method = "POST"
        if self.stream_name =='Tasks':
            list_id = record.get('list_id', self.config.get('list_id'))
            if list_id:
                stream = f"list/{list_id}/task"
                mapping = {}
                if "id" in record:
                    stream = f"task/{record['id']}"
                    method = "PUT"       
                if record.get("subject"):
                    mapping["name"] = record.get("subject")
                if record.get("description"):
                    mapping["description"] = record.get("description")
                if record.get("status"):
                    mapping["status"] = record.get("status")
                if record.get("assignees"):
                    mapping["assignees"] = self.get_assigness(record.get('assignees'))
                if record.get('due_date'):
                    due_date = parse(record.get('due_date'))
                    mapping.update({"due_date":round(time.mktime(due_date.timetuple())*1000)})
                res = self._request(mapping,stream, request_type=method)
                if "id" in res:
                    if "id" not in record:
                        print(f"Task with id {res['id']} successfully added.")
                    else:
                        print(f"Task with id {res['id']} successfully updated.")
            else:
                print(f"List id missing for task {record.get('subject')} skipping.")        